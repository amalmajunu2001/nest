import sys
from nest.topology import *
from nest.experiment import *
from nest.topology.helpers.star_topology import Star



# Initializing parameters
num_nodes = 4
ip_addr = "192.168.1.0/24" 

# Create the star topology
star = Star(num_nodes, ip_addr)
nodes = star.nodes
node_switch_interface = star.node_switch_interface
nodes[0].ping(node_switch_interface[1][0].get_address())

