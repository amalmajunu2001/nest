from nest.experiment import *
from nest.topology import *

########################################################################################
#
# Topology: Star
# Paramters: Number of nodes/spokes, IP address of the network
# 
#
#                              h0     h1
#                               \    /
#                                \  /
#                      h5 ------- s1 ------ h2
#                                /  \
#                               /    \
#                             h4      h3
#
########################################################################################


class Star:
    def __init__(self, num_spokes, ip_addr):
        self.num_spokes = num_spokes

	# List to store the node instances
        self.nodes = []		
        	

        # Creating all the nodes and storing in a list
        for node in range(self.num_spokes):
            self.nodes.append(Node("Node-" + str(node)))

	# Creating the switch for connecting all the nodes
        self.switch = Switch("switch")

        # List to store connection between node and switch as a tuple 
        self.node_switch_interface = []

        # [ (x y) ] -> x represents the node interface, y represents the switch interface
        for i in range(self.num_spokes):
            self.node_switch_interface.append(
                connect(self.nodes[i], self.switch)) 

        # The IP address of the network
        self.ip_addr = Subnet(ip_addr)

        for i in range(self.num_spokes):
            # Assigning addresses to the interfaces
            self.node_switch_interface[i][0].set_address(
                self.ip_addr.get_next_addr())
            self.node_switch_interface[i][1].set_address(
                self.ip_addr.get_next_addr())

